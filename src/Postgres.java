import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Postgres {

	static Connection con = null;
	static Statement st = null;
	static ResultSet rs = null;

	// devuelve el mayor id e incrementa en 1
	public static int id() throws ClassNotFoundException, SQLException {
		int maxId = 0;
		Class.forName("org.postgresql.Driver");

		con = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/gpx", "test", "test");

		st = con.createStatement();
		rs = st.executeQuery("SELECT max(id) from usuarios");
		if (rs != null)
			while (rs.next()) {
				maxId = rs.getInt(1) + 1;
			}

		return maxId;

	}

	// comprueba si el usuario existe
	public static boolean existe(String user) throws ClassNotFoundException,
			SQLException {
		Class.forName("org.postgresql.Driver");

		con = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/gpx", "test", "test");

		st = con.createStatement();
		rs = st.executeQuery("SELECT nombre from usuarios");
		if (rs != null)
			while (rs.next()) {
				if (user.equals(rs.getString("nombre"))) {
					return true;
				}
			}
		return false;
	}

	public static void nuevoUsuario(int id, String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.postgresql.Driver");

		con = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/gpx", "test", "test");

		st = con.createStatement();
		// si no existe el usuario, añádelo
		if (!existe(user)) {
			st.executeUpdate("INSERT into usuarios VALUES(" + id + ",'" + user
					+ "','" + password + "');");
		}
	}
}

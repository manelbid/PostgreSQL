import com.db4o.ObjectServer;
import com.db4o.cs.Db4oClientServer;


public class DB4Object implements Runnable {
	private boolean stop = false;

	public void run() {
		synchronized (this) {
			ObjectServer server =
Db4oClientServer.openServer(Db4oClientServer.newServerConfiguration(), "Nserver.yap", 8733);

			Thread.currentThread().setName(this.getClass().getName());

			
			
			server.grantAccess("user1", "password");
			server.grantAccess("user2", "password");
			try {

				while (!stop) {
					System.out.println("SERVER:[" + System.currentTimeMillis() + "] Server's running... ");
					this.wait(60000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				server.close();
			}
		}
	}
}

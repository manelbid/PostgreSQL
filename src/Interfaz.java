import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 *
 */

public class Interfaz extends JFrame implements ActionListener {
	private JPasswordField pwdPass;
	private JTextField tfUser;

	JLabel info;
	JLabel picture;
	JButton btnClear;
	JButton btnOk;
	static String u;
	static String p;

	public Interfaz() {

		this.setBounds(300, 300, 450, 400);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setTitle("AÑADIR NUEVO USUARIO");
		this.setVisible(true);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.WHITE);
		this.getContentPane().add(desktopPane, BorderLayout.CENTER);

		pwdPass = new JPasswordField();
		pwdPass.setBounds(205, 151, 133, 27);
		desktopPane.add(pwdPass);
		pwdPass.setName("pwdPass");
		pwdPass.setToolTipText("Introduce contrasenya");

		JLabel lblContrassenya = new JLabel("Contrasenya");
		lblContrassenya.setBounds(82, 157, 105, 15);
		desktopPane.add(lblContrassenya);

		JLabel lblUsuari = new JLabel("Usuario");
		lblUsuari.setBounds(82, 101, 105, 15);
		desktopPane.add(lblUsuari);

		tfUser = new JTextField();
		tfUser.setBounds(205, 95, 133, 27);
		desktopPane.add(tfUser);
		tfUser.setName("tfUser");
		tfUser.setToolTipText("Introduce nombre de usuario");

		btnClear = new JButton("Borrar datos");
		btnClear.setBounds(57, 275, 172, 25);
		desktopPane.add(btnClear);
		btnClear.addActionListener(this);
		btnClear.setToolTipText("Limipia el formulario");
		btnClear.setName("btnClear");

		btnOk = new JButton("OK");
		btnOk.setBackground(new Color(60, 179, 113));
		btnOk.setBounds(325, 259, 68, 56);
		desktopPane.add(btnOk);
		btnOk.setName("btnOk");
		btnOk.addActionListener(this);
		btnOk.setToolTipText("Valida los datos");
		btnOk.setMnemonic(KeyEvent.VK_ENTER);
		btnOk.setVisible(true);

		info = new JLabel();
		info.setHorizontalAlignment(SwingConstants.CENTER);
		info.setForeground(Color.RED);
		info.setBounds(76, 219, 292, 15);
		desktopPane.add(info);
		info.setName("info");

	}

	public static void main(String[] args) {

		new Interfaz();
	}

	@Override
	public void actionPerformed(ActionEvent login) {

		// Clear data
		if (login.getSource() == btnClear) {
			tfUser.setText("");
			pwdPass.setText("");
			info.setText("");
		} else {
			// si la contraseña tiene un mínimo de 6 caracteres, acepta
			try {
				if (!tfUser.getText().equals("") && !pwdPass.getText().equals("")) {
					if (Postgres.existe(tfUser.getText())) {
						info.setText("El usuario ya existe");
					} else {
						if (pwdPass.getText().length() >= 6) {
							// Validate the information
							try {
								Postgres.nuevoUsuario(Postgres.id(),
										tfUser.getText(), pwdPass.getText());
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							info.setText("Mínimo 6 caracteres en contraseña");
						}
					}
				} else {
					info.setText("Introduce usuario y contraseña");
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
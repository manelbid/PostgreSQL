import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;

public class PanelControl extends JFrame implements ActionListener {

	static String nombre;
	Vector files;
	Vector fila;

	
	JButton muestraDatos;
	JButton anadir;
	JButton borrar;
	JTable taula;


	public PanelControl() throws ClassNotFoundException, SQLException {

		this.setBounds(600, 600, 900, 900);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("PANEL CONTROL");
		this.setVisible(true);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.WHITE);
		desktopPane.setLayout(new BorderLayout());
		this.getContentPane().add(desktopPane);

		muestraDatos = new JButton("Mostrar datos");
		muestraDatos.addActionListener(this);
		
		anadir = new JButton("Añadir");
		anadir.addActionListener(this);
		
		borrar = new JButton("Borrar");
		borrar.addActionListener(this);
		
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		this.getContentPane().add(toolBar, BorderLayout.WEST);
		toolBar.add(muestraDatos);
		toolBar.add(anadir);
		toolBar.add(borrar);

		Vector columnes = new Vector();

		columnes.add("Apellido");
		columnes.add("Nombre");

		files = new Vector();
		// recupera los datos de la BD y los muestra en una tabla
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		Class.forName("org.postgresql.Driver");

		con = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/gpx", "test", "test");

		st = con.createStatement();
		rs = st.executeQuery("SELECT nombre from usuarios");
		if (rs != null) {
			while (rs.next()) {
				nombre = rs.getString(1);
				fila = new Vector();
				fila.add(nombre);
				files.add(fila);
			}
		}

		taula = new JTable(files, columnes);
		desktopPane.add(taula);
		JScrollPane panel = new JScrollPane(taula);
		desktopPane.add(panel, BorderLayout.CENTER);

	}

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException {

		new PanelControl();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == borrar) {
			Connection con = null;
			Statement st = null;
			ResultSet rs = null;
			try{
			Class.forName("org.postgresql.Driver");

			con = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/gpx", "test", "test");

			st = con.createStatement();
			rs = st.executeQuery("DELETE from usuarios WHERE nombre = '"+taula.getValueAt(taula.getSelectedRow(), taula.getSelectedColumn())+"'");
			} catch (Exception ex){
				
			}
			DefaultTableModel model = (DefaultTableModel) taula.getModel();
			model.removeRow(taula.getSelectedRow());
		} else if (e.getSource() == anadir){
			Interfaz i = new Interfaz();
		}
	} 
}

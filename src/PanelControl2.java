import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;

public class PanelControl2 extends JFrame implements ActionListener {

	static String nombre;
	JDesktopPane desktopPane;
	Vector files;
	Vector fila;
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	boolean tabla = false;

	JButton muestraDatos;
	JButton borrar;
	JTable taula;
	Vector columnes;

	public PanelControl2() throws ClassNotFoundException, SQLException {

		this.setBounds(600, 600, 900, 900);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("PANEL CONTROL");
		this.setVisible(true);

		desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.WHITE);
		desktopPane.setLayout(new BorderLayout());
		this.getContentPane().add(desktopPane);

		muestraDatos = new JButton("Mostrar datos");
		muestraDatos.addActionListener(this);

		borrar = new JButton("Borrar");
		borrar.addActionListener(this);

		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		this.getContentPane().add(toolBar, BorderLayout.WEST);
		
		toolBar.add(borrar);
		toolBar.add(muestraDatos);

		columnes = new Vector();

		columnes.add("Apellido");
		columnes.add("Nombre");

		files = new Vector();
		// recupera los datos de la BD y los muestra en una tabla

		Class.forName("org.postgresql.Driver");
		con = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/gpx", "test", "test");
		st = con.createStatement();

		

	}

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException {

		new PanelControl2();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == borrar) {
			try {
				rs = st.executeQuery("DELETE from usuarios WHERE nombre = '"
						+ taula.getValueAt(taula.getSelectedRow(),
								taula.getSelectedColumn()) + "'");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			DefaultTableModel model = (DefaultTableModel) taula.getModel();
			model.removeRow(taula.getSelectedRow());
		} else if (e.getSource() == muestraDatos) {
			if (tabla){
				DefaultTableModel model2 = (DefaultTableModel) taula.getModel();
				int f = taula.getRowCount();
				for (int i = 0; f > i; i++) {
					model2.removeRow(0);
				}
			}
			try {
				rs = st.executeQuery("SELECT nombre from usuarios");
				if (rs != null) {
					while (rs.next()) {
						nombre = rs.getString(1);
						fila = new Vector();
						fila.add(nombre);
						files.add(fila);
					}
				}
				taula = new JTable(files, columnes);
				desktopPane.add(taula);
				JScrollPane panel = new JScrollPane(taula);
				desktopPane.add(panel, BorderLayout.CENTER);
				tabla = true;
				
			} catch (Exception ex) {

			}
		}
	}
}

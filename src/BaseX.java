import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.InfoDB;
import org.basex.core.cmd.Open;

public class BaseX {

	public static void main(String[] args) {

		try {

			BaseXServer server = new BaseXServer();

			ClientSession session = new ClientSession("localhost", 1984,
					"admin", "admin");

			// session.execute(new CreateDB("database"));
			session.execute("create db database");

			// session.execute(new Open("database"));
			// session.execute("open database");
			Path path = Paths.get("books.xml");
			InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));

			session.add("books.xml", is);

			is = new ByteArrayInputStream(
					"<pruebas>asdasdasdas</pruebas>".getBytes());
			session.add("pruebas.xml", is);

			System.out.println(session.info());
			System.out.println(session.execute(new InfoDB()));

			
			System.out
					.println(session.execute("xquery collection('database')"));

			System.out.println(session.execute("list database"));

			is = new ByteArrayInputStream(
					"<reemplazar>asdasdasdas</reemplazar>".getBytes());
			session.replace("pruebas.xml", is);

			// System.out.println(session.execute("xquery doc('pruebas.xml')"));
			System.out.println(session
					.execute("xquery collection('database/pruebas.xml')"));

			/*
			 * 
			 * String consulta = " for $x in //book " + " where $x/price <= 30 "
			 * + " order by $x/category " + " return $x";
			 * 
			 * 
			 * 
			 * //book/@category ClientQuery query = session.query(consulta);
			 * System.out.println(query.execute());
			 */

			server.stop();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
